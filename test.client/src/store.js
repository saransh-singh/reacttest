// import npm packages
import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { combineReducers } from 'redux'

// import local files
import sagas from './sagas/index'
import { reducer as transactions } from "./reducers/transactionReducer";
import { reducer as transactionTypes } from "./reducers/transactionTypeReducer";
const sagaMiddleware = createSagaMiddleware()
let composeEnhancers = compose

if (process.env.NODE_ENV === 'development') {
  const composeWithDevToolsExtension =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  if (typeof composeWithDevToolsExtension === 'function') {
    composeEnhancers = composeWithDevToolsExtension
  }
}

const reducers = combineReducers({
  transactions,
  transactionTypes,
})

export default createStore(
  reducers,
  composeEnhancers(applyMiddleware(sagaMiddleware))
)

sagaMiddleware.run(sagas)