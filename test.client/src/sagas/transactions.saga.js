// import npm packages
import { call, put } from "redux-saga/effects";

// import local files
import { getTransactions, postTransaction } from '../apis/transactions';
import { receiveTransactions, receiveTransactionAdd } from '../actions/transactions';

function* transactions(action) {
	try {
		const data = yield call(getTransactions, action.data);
		yield put(receiveTransactions(data));
	} catch (e) {
		yield put(receiveTransactions({error: e.message || 'Something went worng'}));
	}
}

function* addtransaction(action) {
	try {
		const data = yield call(postTransaction, action.data);
		yield put(receiveTransactionAdd(data));
	} catch (e) {
		yield put(receiveTransactionAdd({error: e.message || 'Something went worng'}));
	}
}

export const transactionsSaga = {
	transactions,
	addtransaction,
};