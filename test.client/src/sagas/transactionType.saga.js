// import npm packages
import { call, put } from "redux-saga/effects";

// import local files
import { getTransactionTypes } from '../apis/transactionTypes';
import { receiveTransactionTypes } from '../actions/transactionType';

function* transactionTypes(action) {
	try {
		const data = yield call(getTransactionTypes, action.data);
		yield put(receiveTransactionTypes(data));
	} catch (e) {
		yield put(receiveTransactionTypes({error: e.message || 'Something went worng'}));
	}
}

export const transactionTypesSaga = {
	transactionTypes,
};