// import npm packages
import { takeLatest } from "redux-saga/effects";

// import local files
// ***************** import actions
import {
  REQUEST_TRANSACTION,
  REQUEST_TRANSACTION_ADD,
} from '../types/transaction';

import {
  REQUEST_TRANSACTION_TYPE,
} from '../types/transactionType';



// **************** import sagas
import {
  transactionsSaga
} from "./transactions.saga";

import {
  transactionTypesSaga
} from './transactionType.saga';


export default function* root() {
  yield takeLatest(REQUEST_TRANSACTION_ADD, transactionsSaga.addtransaction);
  yield takeLatest(REQUEST_TRANSACTION, transactionsSaga.transactions);

  yield takeLatest(REQUEST_TRANSACTION_TYPE, transactionTypesSaga.transactionTypes);
}