// import npm packages
import { combineReducers } from "redux";

// import local files
import { RECEIVE_TRANSACTION_TYPE } from "../types/transactionType";

const transactionTypes = (state = {}, { type, data }) => {
	switch (type) {
		case RECEIVE_TRANSACTION_TYPE:
			return data;
		default:
			return state;
	}
};

export const reducer = combineReducers({
	transactionTypes
});