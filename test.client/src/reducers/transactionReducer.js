// import npm packages
import { combineReducers } from "redux";

// import local files
import { RECEIVE_TRANSACTION, RECEIVE_TRANSACTION_ADD } from "../types/transaction";

const transactions = (state = {list: {}, addStatus: {}}, { type, data }) => {
	switch (type) {
		case RECEIVE_TRANSACTION:
			return {
				...state,
					list: data
			};
		case RECEIVE_TRANSACTION_ADD:
			return {
				...state,
				addStatus: data
			};
		default:
			return state;
	}
};

export const reducer = combineReducers({
	transactions
});