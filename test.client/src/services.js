import axios from "axios";

export class Services {
  static getCall(url, auth) {
    if (!auth) {
      return axios.get(url);
    } else {
      return axios.get(url);
    }
  }

  static postCall(url, params, auth) {
    if (!auth) {
      return axios.post(url, params);
    } else {
      return axios.post(url, params);
    }
  }

  static putCall(url, params, auth) {
    if (!auth) {
      return axios.put(url, params);
    } else {
      return axios.put(url, params);
    }
  }

  static deleteCall(url, auth, bodyData) {
    if (bodyData) {
      return axios.delete(url);
    } else {
      return axios.delete(url);
    }
  }
}
