import { Services } from "../services";
import * as config from "../config";

export const getTransactionTypes = async data => {
  const url = `${config.BASE_URL}/api/transactionType/`;
  let res = await Services.getCall(url, true);
  return { data: res.data };
};
