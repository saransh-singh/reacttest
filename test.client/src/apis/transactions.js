import { Services } from "../services";
import * as config from "../config";

export const getTransactions = async data => {
  const url = `${config.BASE_URL}/api/transaction/${data.dbName}/${data.collectionName}`;
  let res = await Services.getCall(url, true);
  return { data: res.data };
};

export const postTransaction = async data => {
  const url = `${config.BASE_URL}/api/transaction/`;
  
  let res = await Services.postCall(url, data, true);
  return { data: res.data };
};