import {
	REQUEST_TRANSACTION,
    RECEIVE_TRANSACTION,
    REQUEST_TRANSACTION_ADD,
    RECEIVE_TRANSACTION_ADD,
} from "../types/transaction";


export const requestTransactions = data => ({
    type: REQUEST_TRANSACTION, data
});

export const receiveTransactions = data => ({
    type: RECEIVE_TRANSACTION, data
});

export const requestTransactionAdd = data => ({
    type: REQUEST_TRANSACTION_ADD, data
});

export const receiveTransactionAdd = data => ({
    type: RECEIVE_TRANSACTION_ADD, data
});