import {
	REQUEST_TRANSACTION_TYPE,
  RECEIVE_TRANSACTION_TYPE,
} from "../types/transactionType";


export const requestTransactionTypes = data => ({
    type: REQUEST_TRANSACTION_TYPE, data
});

export const receiveTransactionTypes = data => ({
    type: RECEIVE_TRANSACTION_TYPE, data
});