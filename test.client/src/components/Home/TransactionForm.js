// import npm packages
import React, { Component } from 'react';
import {
	Card,
	CardBody,
	Container,
	Form,
	FormGroup,
	Label,
	Col,
	Input,
  Button,
  CardHeader,
} from 'reactstrap';
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";

// import local files
import { requestTransactionAdd, receiveTransactionAdd, requestTransactions } from '../../actions/transactions';


class TransactionForm extends Component {
	constructor(props) {
		super(props);
		this.state = {
      dbData: '',
      data: {}
		}
  }
  
  componentDidUpdate() {
    if(this.props.transactionAddStatus === 200) {
      this.props.receiveTransactionAdd({});
      
    }
  }
  onUploadImage = (event, callback) => {
    event.preventDefault();
    const file = event.target.files[0];
    const reader = new window.FileReader();
    const extension = file.name.split(".").pop();
    const imageArr = ["pdf"];

    if (file) {
        if (imageArr.indexOf(extension.toLowerCase()) === -1) {
            alert('Invalid image file extension');
            //event.currentTarget.value = "";
            event.target.value = "";
            return false;
        }
        reader.onload = fileLoadEvent => {
            this.setState({
              base64: {
                extension: extension,
                fileData: reader.result
              }
            });
        };
        reader.readAsBinaryString(event.currentTarget.files[0]);
    }
  }

  renderForm(fields) {
    return (
      <Form
        onSubmit={(e) => {
          e.preventDefault()
          const { data } = this.state;
          data.dbName = this.props.dbData.dbName;
          data.collectionName = this.props.dbData.collectionName;
          this.props.requestTransactionAdd(data)
        }}
      >
        {
          fields.map((val, index) => (
            <FormGroup row key={index}>
              <Label for={val.name} sm={3}>{val.name && val.name.toUpperCase()}</Label>
              <Col sm={9}>
                <Input
                  type={val.field}
                  name={val.name}
                  id={val.name}
                  required
                  onChange={(e) => {
                    const { data } = this.state;
                    data[val.name] = e.target.value;
                    this.setState({
                      data
                    });
                  }}
                />
              </Col>
            </FormGroup>
          ))
        }
        <FormGroup>
          <Button>Submit</Button>
        </FormGroup>
      </Form>
    )
  }


	render() {
		return (
			<Container>
        <Card
          style={{
            padding: '1%',
              background: 'aliceblue',
          }}
        >
          <CardHeader
            tag="h3"
            style={{
              background: 'none',
              borderBottom: 'none'
            }}
          >
            Add Transactions
          </CardHeader>
          <CardBody> 
            {
              this.props.dbData
              && this.props.dbData.fields
              && this.props.dbData.fields.length > 0
              && this.renderForm(this.props.dbData.fields)
            }
          </CardBody>
        </Card>
      </Container>
		)
	}
}

const mapStatetoProps = state => {
  return {
    transactionAddStatus: state.transactions
      && state.transactions.transactions
      && state.transactions.transactions.addStatus
      && state.transactions.transactions.addStatus.data
      && state.transactions.transactions.addStatus.data.status,
    transactionTypes: (state.transactionTypes.transactionTypes
      && state.transactionTypes.transactionTypes.data
      && state.transactionTypes.transactionTypes.data.data) || [],
  }
}

const mapDispatchToProps = dispatch => (
  bindActionCreators({
    requestTransactionAdd,
    requestTransactions,
    receiveTransactionAdd,
  }, dispatch)

)


export default connect(mapStatetoProps, mapDispatchToProps)(TransactionForm);