// import npm packages
import React, { Component } from 'react';
import {
	Card,
	CardBody,
  Container,
  CardHeader,
	Table
} from 'reactstrap';
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";

// import local files

import * as config from '../../config';

class TransactionsList extends Component {
	componentDidMount() {
		
	}
	render() {
		return (
			<Container>
        <Card
        style={{
          padding: '1%',
            background: 'aliceblue',
        }}
        >
          <CardHeader
            tag="h3"
            style={{
              background: 'none',
              borderBottom: 'none'
            }}
          >
            Transactions List
          </CardHeader>
          <CardBody style={{height: '334px', overflow: 'auto'}}>
            <Table>
              <thead>
                <tr>
                  <th>Transaction Id</th>
                  <th>Source</th>
                  <th>Destination</th>
                  <th>Transaction Type</th>
                  <th>Money / File</th>
                </tr>
              </thead>
              <tbody>
                {
                  this.props.transactions ?
                    this.props.transactions.length > 0 ?
                      this.props.transactions.map(transaction => (
                        <tr key={transaction._id}>
                          <th scope="row">{transaction._id}</th>
                          <td>{transaction.source}</td>
                          <td>{transaction.destination}</td>
                          <td>{transaction.transactionType}</td>
                          <td>
                            {
                              transaction.transactionType === 'money' ?
                                transaction.money
                              :
                                <a href={`${config.BASE_URL}${transaction.filePath}`} >{transaction.filePath}</a>
                            }
                          </td>
                        </tr>
                      ))
                    :
                      <tr>
                        <td>
                          No Transaction Found
                        </td>
                      </tr>
                  :
                    <tr>
                      <td>
                        Loading
                      </td>
                    </tr>
                }
                
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </Container>
		)
	}
}


const mapStatetoProps = state => {
  return {
    transactions: state.transactions
    && state.transactions.transactions
    && state.transactions.transactions.list
    && state.transactions.transactions.list.data
    && state.transactions.transactions.list.data.data,
  }
}


// const mapDispatchToProps = dispatch => (
//   bindActionCreators({ requestTransactions }, dispatch)

// )


export default connect(mapStatetoProps, null)(TransactionsList);