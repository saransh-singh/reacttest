// import npm packages
import React, { Component } from 'react';
import {
	Row,
	Container,
	Card,
	CardBody,
	FormGroup,
	Label,
	Col,
	Input
} from 'reactstrap';
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";

// import local files
import TransactionsList from './TransactionsList';
import TransactionForm from './TransactionForm';
import { requestTransactionTypes } from '../../actions/transactionType';
import { requestTransactions } from '../../actions/transactions';

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
      dbData: '',
		}
  }
  componentDidMount() {
    this.props.requestTransactionTypes();
	}
	
	render() {
		return (
			<React.Fragment>
				<Container>
					<Card
						style={{
							padding: '1%',
							background: 'aliceblue',
							marginTop: '80px',
						}}
					>
						<CardBody> 
							<FormGroup row>
								<Label for="transaction-type" sm={3}>Adapter Type</Label>
								<Col sm={9}>
									<Input
										type="select"
										name="select"
										id="transaction-type"
										required
										onChange={(e) => {
											const index = this.props.transactionTypes.findIndex(val => val._id === Number(e.target.value));
											if(index !== -1) {
												this.setState({
													dbData: this.props.transactionTypes[index]
												},() => {
													this.props.requestTransactions({
														dbName: this.props.transactionTypes[index].dbName,
														collectionName: this.props.transactionTypes[index].collectionName
													});
												})
											}
										}}
									>
										<option value=''>-- Select one adapter --</option>
										{
											this.props.transactionTypes.map(transactionType => (
												<option value={transactionType._id} key={transactionType._id}>{transactionType.dbName && transactionType.dbName.toUpperCase()}</option>
											))
										}
									</Input>
								</Col>
							</FormGroup>
						</CardBody>
					</Card>
				</Container>
				{
					this.state.dbData && (
						<Row  style={{marginTop: '3%'}}>
							<Col lg={6} md={6} sm={12}>
								<TransactionForm dbData={this.state.dbData}/>
							</Col>
							<Col lg={6} md={6} sm={12}>
								<TransactionsList dbData={this.state.dbData} />
							</Col>
						</Row>
					)
				}
				
			</React.Fragment>
		)
	}
}


const mapStatetoProps = state => {
  return {
    transactionTypes: (state.transactionTypes.transactionTypes
      && state.transactionTypes.transactionTypes.data
      && state.transactionTypes.transactionTypes.data.data) || [],
  }
}

const mapDispatchToProps = dispatch => (
  bindActionCreators({
		requestTransactionTypes,
		requestTransactions
  }, dispatch)

)


export default connect(mapStatetoProps, mapDispatchToProps)(Home);