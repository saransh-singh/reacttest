'use strict';

var db = require('../../config/connections')

/***************************************************************************************************************************
 * list transactions
  with pagination and filter
 * API /api/transaction/
 * METHOD POST 
 * Secuirty -> Open
 **************************************************************************************************************************/
exports.index = function(req, res) {
  if(!req.params.dbName) {
    return  Services._handleError(req, res, 'no db name found');
  }

  db.connectDB('money');

  const { params } = req;
  const { dbName, collectionName } = params;
  
  var mongoose = require('mongoose'),
    Services = require('../../service'),
    Schema = mongoose.Schema;

  var Collection = mongoose.connections[Services.getObjectIndex(mongoose.connections, "name", dbName)].model(collectionName, new Schema({
    to: {type: String},
    from: {type: String},
    money: {type: String},
    file: {type: String}
  }));  

    Collection.find()
    .then(transactions => {
        return Services._response(res, transactions);
    })
    .catch(err => {
        console.log(err);
        return Services._handleError(req, res, err);
      });
};

exports.post = function(req, res) {
  try{
    if(!req.body.dbName) {
      return  Services._handleError(req, res, 'no db name found');
    }
    db.connectDB('money');

    const { body } = req;
    const { dbName, collectionName } = body;
    
    var mongoose = require('mongoose'),
      Services = require('../../service'),
      Schema = mongoose.Schema;
    var Collection = mongoose.connections[Services.getObjectIndex(mongoose.connections, "name", dbName)].model(collectionName, new Schema({
      to: {type: String},
      from: {type: String},
      money: {type: String},
      file: {type: String}
    }));  

    delete body.dbName;
    delete body.collectionName;

    if(req.body.base64) {
      const docUrl = `/client/assets/file/${Math.floor(100000 + Math.random() * 900000)}.${req.body.base64.extension}`;
      // create file url from server directory
      const url = `${process.env["PWD"]}${docUrl}`;

      const documentFile = { url, fileData: req.body.base64.fileData };
      // upload file on server
      file.upload(documentFile, (isFileUpload) => {
        // check file upload on server successfully or not
        if (isFileUpload) {
          // update  file url in db
          
          const collection = new Collection(body)
          collection
            .save()
            .then(result => {
              mongoose.connection.close();
              return Services._response(res, result);
            })
            .catch(err => {
              mongoose.connection.close();
              return Services._handleError(req, res, err);
            });
        } else {
          mongoose.connection.close();
          Services._handleError(req, res, {message: 'file not uploaded'});
        }
      });
      return null;
    }
    
    const collection = new Collection(body)
    collection
    .save()
    .then(result => {
      mongoose.connection.close();
      return Services._response(res, result);
    })
    .catch(err => {
      mongoose.connection.close();
      return Services._handleError(res, err);
    });
  } catch(e) {
    mongoose.connection.close();
    return Services._handleError(res, e);
  }
}