'use strict';

var express = require('express');
var controller = require('./transaction.controller');
var router = express.Router();

//Sub routes of client module
router.get('/:dbName/:collectionName', controller.index);
router.post('/', controller.post);

module.exports = router;