'use strict';

var express = require('express');
var controller = require('./transactionType.controller');
var router = express.Router();

//Sub routes of client module
router.get('/', controller.index);

module.exports = router;