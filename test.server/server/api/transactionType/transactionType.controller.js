'use strict';

var Services = require('../../service');
var TransactionType = require('./transactionType.model');
var mongoose = require('mongoose');
var _ = require('lodash');
var CSV = require("csvtojson");
var async = require('async');
var request = require('request');
var file = require('../../service/fileSystem')
/***************************************************************************************************************************
 * list transactions
  with pagination and filter
 * API /api/transaction/
 * METHOD POST 
 * Secuirty -> Open
 **************************************************************************************************************************/
exports.index = function(req, res) {
  var db = require('../../config/connections');
  var config = require('../../config/environment');
  // mongoose.connection.close();
  // db.connectDB(config.mongo.TODO_APP_DB);

    TransactionType.find()
    .then(transactionTypes => {
      mongoose.connection.close();
      return Services._response(res, transactionTypes);
    })
    .catch(err => {
      console.log(err);
      mongoose.connection.close();
      return Services._handleError(req, res, err);
    });
};
