(function() {
    'use strict';
    var mongoose = require('mongoose'),
        Services = require('../../service'),
        ENUMS = require('../../enums'),
        _ = require('lodash'),
        AutoIncrement = require('mongoose-sequence')(mongoose),
        Schema = mongoose.Schema,
        TransactionTypeSchema = new Schema({
            _id: { type: Number },
            adapter: {type: String},
            dbName: {type: String},
            collectionName: {type: String},
            fields: {type: Array},
        }, { timestamps: true });

        TransactionTypeSchema.plugin(AutoIncrement, { id: ENUMS.TODO_APP_DB_COLLECTIONS.TRANSACTION_TYPE.name });

    /***************************************************************************************************************************
     * Pre-save hook
     **************************************************************************************************************************/


    module.exports = mongoose.connections[Services.getObjectIndex(mongoose.connections, "name", process.env.TODO_APP_DB_NAME)]
        .model(ENUMS.TODO_APP_DB_COLLECTIONS.TRANSACTION_TYPE.name, TransactionTypeSchema, ENUMS.TODO_APP_DB_COLLECTIONS.TRANSACTION_TYPE.name);
}());